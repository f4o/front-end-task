var webJS = {
  common: {}
};

webJS.common = (function (window, document) {
  function formValidation() {
    var form = $('.form');
    var $this,
        timeout,
        keyCode = null;
    var timeout = null;
    var parentContainer = {
      step1 : 'step--1',
      step2 : 'step--2',
      step3 : 'step--3'
    };
    var errorMessages = {
      cardCode: {
        numberOfChars  : 'Please enter exactly 13 characters',
        alphanumOnly   : 'Only letters and digits allowed!',
        digitsMissing  : 'At least one digit required!',
        lettersMissing : 'At least one letter required!'
      },
      name: {
        empty: 'This field cannot be empty.',
        minChars: 'Enter at least two characters',
        charType: 'Please enter only letters'
      },
      email : {
        charType: 'Provide correct email address.'
      }
    };

    // validate on click on step1
    form.on('click', 'input', function(e) {
      $this = $(this);
      e.preventDefault();
      // check if clicking on button
      if (e.currentTarget.type.toLowerCase() == 'button' && $this.parents('.step').hasClass(parentContainer.step1)) validateFields($this, $(this).prev().val(), e.type, 'step1')
    });

    // validate on keydown and focusout enter on step1
    form.on('keydown focusout', 'input', function(e) {
      $this = $(this);
      if ((typeof e.key !== 'undefined' && e.key.toLowerCase() == 'enter') || e.type == 'focusout') {
        clearTimeout(timeout);
        timeout = setTimeout(() => { delegateValidation(e, $(this)) }, 500);
      }
    });

    // validate on form submit
    form.on('submit', function(e) {
      e.preventDefault();
    });

    form.on('click', '.form-submit', function(e) {
      if ($(this).prop('disabled') === false) {
        alert('Congrats!');
      }
    });

    // delegate validation to another function because of timeout not setting $(this) properly
    function delegateValidation(event, $this) {
      validateFields($this, $this.val(), event.type, typeof event.key !== 'undefined' ? event.key.toLowerCase() : false, 'step1');
    }

    // validate fields function
    function validateFields(selector, fieldValue, eventType, keyTyped, step) {
      if (selector.parents('.step').hasClass(parentContainer.step1)) {
        // if user is on step1
        if (checkRegex('cardCode', fieldValue) !== true) {
          selector
            .addClass('form__error')
            .next().addClass('form__error')
            .parents('.step')
            .addClass('step--error')
            .find('.form__error').text(checkRegex('cardCode', fieldValue));
        } else {
          selector
            .removeClass('form__error')
            .next().removeClass('form__error')
            .parents('.step').removeClass('step--error')
            .find('.form__error').text('');
          if (eventType == 'click' || keyTyped == 'enter') {
            selector.parents('.step').addClass('hidden').next().removeClass('hidden');
          }
        }
      } else if (selector.parents('.step').hasClass(parentContainer.step2)) {
        // if user is on step2
        if (selector.hasClass('form-name')) {
          if (checkRegex('name', fieldValue) !== true) {
            selector.parents('.step').addClass('step--error');
            selector.addClass('form__error');
            selector.next().text(checkRegex('name', fieldValue));
          } else {
            selector.removeClass('form__error');
            selector.next().text('');
          }
        } else if (selector.hasClass('email')) {
          if (checkRegex('email', fieldValue) !== true) {
            selector.parents('.step').addClass('step--error');
            selector.addClass('form__error');
            selector.next().text(checkRegex('email', fieldValue));
          } else {
            selector.removeClass('form__error');
            selector.next().text('');
          }
        }

        // check if inputs have errors
        if (!form.find('input').hasClass('form__error') && form.find('input').val() !== '') {
          form.find('.form-submit').prop('disabled', false);
        } else {
          form.find('.form-submit').prop('disabled', true);
        }
      }
    }

    // regex checker function
    // return error message or true
    function checkRegex(regexType, fieldValue) {
      if (regexType === undefined || regexType == 'undefined' || fieldValue === undefined || fieldValue == 'undefined') return;

      if (regexType == 'cardCode') {
        if (fieldValue.match(/[^0-9a-z]/i)) {
          return errorMessages.cardCode.alphanumOnly;
        } else if (!fieldValue.match(/\d/)) {
          return errorMessages.cardCode.digitsMissing;
        } else if (!fieldValue.match(/[a-z]/i)) {
          return errorMessages.cardCode.lettersMissing;
        } else if (fieldValue.length < 13 || fieldValue.length > 13) {
          return errorMessages.cardCode.numberOfChars;
        } else {
          return true;
        }
      } else if (regexType == 'name') {
        if (fieldValue.length < 1) {
          return errorMessages.name.empty;
        } else if (fieldValue.length < 2) {
          return errorMessages.name.minChars;
        } else if (fieldValue.match(/[^a-z]/i)) {
          return errorMessages.name.charType;
        } else {
          return true;
        }
      } else if (regexType == 'email') {
        if (!fieldValue.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
          return errorMessages.email.charType;
        } else {
          return true;
        }
      }
    }
  }

  return {
    formValidation: formValidation
  };

})(window, document);

$(function () {
  webJS.common.formValidation();
});
