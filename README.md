Hello.

This task will be something quite simple and quick, just to have an overview of your set of skills and eye for detail.

- Create a form with 2 steps which should be:
    - Card Code (input box) and submit button
    - User information (Name, Last Name, E-mail) and submit button
- Create an array of messages for success or error that should have: Title and description.
- If the Card code is a alphanumerical string with 13 characters, the second step should be shown, if not, a random validation (error) message should be shown.
- When the user clicks submit on the second step, a succesfull message should be shown.
- All assets needed are in "design" folder. Final designs are in "visual" folder.
- Mobile design is up to you, we usually work with 2 breakpoints in most of the cases which are < 767px and > 768px.

All details that are not defined in this README file means that author can decide on his/her own.

- bootstraped from https://github.com/yeoman/generator-webapp/
- follow the instructions here https://github.com/yeoman/generator-webapp/#getting-started to build or check the `dist`folder
- final design images were too big so I took some measurments from foreo.com site and went with that
- measurments are approx, replicated what I could in least amount of time
- for the form I used only two breaking points, 767px & 1023px, kept styling to a minimum
- put up a basic jQuery validation based on iput value and DOM element classes
